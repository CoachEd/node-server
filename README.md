# Sample Node.js Web Server

## Getting Started

1. Clone this repository.
1. Download and install [Node.js](https://nodejs.org/en/)
1. Install the required Node.js modules: go into the _node-server_ directory and run `npm install`
1. The server defaults to port 1234. You can edit `config.json` to change the port number.

### Running the Server

```
$ node app.js
server running: http://localhost:1234/  (Ctrl+C to Quit)
```

### Testing the Server

Here are some ways to test the server:

* Add a record using a simple HTML page calling the REST API: [http://localhost:1234/addrecord.html](http://localhost:1234/addrecord.html)
* Read a record using a simple HTML page calling the REST API: [http://localhost:1234/readrecord.html](http://localhost:1234/readrecord.html)
* Open the static homepage in the public folder: http://localhost:1234/index.html
* Try a RESTful GET request: http://localhost:1234/small
* Create a web page to try the RESTful services (or use Chrome POSTMAN)
  * See _routes/routes.js_ for a description of the available RESTful web services

