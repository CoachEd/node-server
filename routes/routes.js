/**
* Defines the different RESTful web services that can be called.
*
* @param (type) app Instance of express
* @returns (undefined) Not used
*/

var appRouter = function(app,fs,port) {
  var filepath = 'data';
  
  //small web page (GET); e.g. http://localhost:1234/small
  app.get('/small', function(req, res) {
  return res.status(200).send('<!DOCTYPE html><html><head><title>Small Page</title></head><body>A really small web page</body></html>');
  });
  
  //add a record (POST); http://localhost:1234/addrecord
  app.post('/addrecord',function(req, res) {
    console.log("POST (addrecord) received.");
  
    //if counter.json does not exist, create it with counter value 0
    if (!fs.existsSync(filepath + '/counter.json')) {
      console.log('creating counter.json first time...');
      fs.writeFileSync(filepath + '/counter.json', JSON.stringify({ "counter": 0 }, null, 2) , 'utf-8');
    }

    var obj = JSON.parse(fs.readFileSync(filepath + '/counter.json', 'utf8'));
    var newrecordid = obj.counter + 1;
    var dte = new Date().toISOString().replace(/\..+/, '') + 'Z';
    var responseJson = {
      "id": newrecordid,
      "created_at": dte,
      "updated_at": dte,
      "subject": req.body.subject,
      "message": req.body.message,
      "randomnumber": req.body.randomnumber
    };
  
    //write record to file
    fs.writeFile(filepath + '/' + newrecordid + '.json', JSON.stringify(responseJson, null, 2) , function(err) {
      if (err) {
        return console.log(err);
      }
    });
  
    //update counter file
    fs.writeFileSync(filepath + '/counter.json', JSON.stringify({ "counter": newrecordid }, null, 2) , 'utf-8');
    console.log('created ' + newrecordid + '.json');
  
    return res.status(200).send(responseJson);
  });
  
  //update a record (PUT); http://localhost:1234/updaterecord/1.json
  app.put('/updaterecord/:id.json',function(req, res) {
    console.log("PUT (updaterecord) received.");  
  
    var recordid = req.params.id;

    // does the file to update even exist?
    if (!fs.existsSync(filepath + '/' + recordid + '.json')) {
      console.log('file does not exist: ' + filepath + '/' + recordid + '.json');
      return res.status(404).send({'message': 'error - ' + recordid + '.json not found'});  
    }  
    var dte = new Date().toISOString().replace(/\..+/, '') + 'Z';
    var responseJson = JSON.parse(fs.readFileSync(filepath + '/' + recordid + '.json', 'utf8'));

    responseJson.subject = req.body.subject;
    responseJson.message = req.body.message;
    responseJson.randomnumber= req.body.randomnumber;
    responseJson.updated_at = dte;
  
    //write updated file
    fs.writeFile(filepath + '/' + recordid + '.json', JSON.stringify(responseJson, null, 2) , function(err) {
      if (err) {
        return console.log(err);
      }
    });

    return res.status(200).send(responseJson);
  });
  
  //read a record (GET); e.g. http://localhost:1234/readrecord/1.json
  app.get('/readrecord/:id.json', function(req, res) {
    console.log("GET (readrecord) received.");
  
    var recordid = req.params.id;
  
    //if {id}.json file does not exist...
    if (!fs.existsSync(filepath + '/' + recordid + '.json')) {
      console.log('file does not exist: ' + filepath + '/' + recordid + '.json');
      return res.status(404).send({'message': 'error - ' + recordid + '.json not found'});
    }  
  
    var retrievedJson = JSON.parse(fs.readFileSync(filepath + '/' + recordid + '.json', 'utf8'));

    res.status(200).send(retrievedJson);
  }); 

  //delete a record (DELETE); http://localhost:1234/deleterecord/1.json
  app.delete('/deleterecord/:id.json', function (req, res) {
    console.log("DELETE (deleterecord) received.");
  
    var recordid = req.params.id;

    // does file exist?
    if (!fs.existsSync(filepath + '/' + recordid + '.json')) {
      console.log('file does not exist: ' + filepath + '/' + recordid + '.json');
      return res.status(404).send({'message': 'error - ' + recordid + '.json not found'});  
    }
   
    fs.unlinkSync(filepath + '/' + recordid + '.json');
      res.status(200).send({'message': 'success - record deleted'});
    });
}

module.exports = appRouter;
